package main

import (
	"math"

	"github.com/bit101/blgo"
	"github.com/bit101/blgo/blmath"
	"github.com/bit101/blgo/geom"
	"github.com/bit101/blgo/random"
	cairo "github.com/ungerik/go-cairo"
)

type FoamCircle struct {
	*geom.Circle
	parent *FoamCircle
}

func NewFoamCircle(x, y, r float64, parent *FoamCircle) *FoamCircle {
	c := geom.NewCircle(x, y, r)
	return &FoamCircle{c, parent}
}

const width = 800.0
const height = 800.0

var circles []*FoamCircle
var todo []*FoamCircle

func main() {
	// random.RandSeed()
	random.Seed(12345)
	surface := blgo.NewSurface(width, height)
	surface.ClearRGB(1, 1, 1)
	surface.SetLineWidth(0.35)
	surface.SetLineCap(cairo.LINE_CAP_ROUND)

	for i := 0; i < 7; i++ {
		c := NewFoamCircle(random.FloatRange(0, width), random.FloatRange(0, height), random.FloatRange(20, 100), nil)
		for !checkCollision(c) {
			c.Center.X = random.FloatRange(0, width)
			c.Center.Y = random.FloatRange(0, height)
			c.Radius = random.FloatRange(20, 100)
		}
		circles = append(circles, c)
		todo = append(todo, c)
	}

	for len(todo) > 0 {
		c := todo[0]
		todo = todo[1:]
		surround(c)
	}

	drawAllCircles(surface)

	surface.WriteToPNG("out.png")
}

func surround(circle *FoamCircle) {
	// size := circle.Radius * 0.3
	size := circle.Radius * blmath.Map(circle.Radius, 1, 100, 0.9, 0.1)
	if size < 1 {
		return
	}
	angle := random.FloatRange(0, math.Pi*2)
	// angle := 0.0
	r := circle.Radius + size
	// inc := math.Asin(size/r) * 2
	inc := math.Asin(size/r) * 2

	for a := angle; a < angle+math.Pi*2; a += inc {
		c := NewFoamCircle(circle.Center.X+math.Cos(a)*r, circle.Center.Y+math.Sin(a)*r, size, circle)
		if checkCollision(c) {
			circles = append(circles, c)
			todo = append(todo, c)
		}
	}
}

func checkCollision(c *FoamCircle) bool {
	for _, c2 := range circles {
		if c.Center.Distance(c2.Center) < c.Radius+c2.Radius-0.1 {
			return false
		}
	}
	return true
}

func drawAllCircles(surface *blgo.Surface) {
	for _, circle := range circles {
		drawCircle(surface, circle)
	}
}

func drawCircle(surface *blgo.Surface, circle *FoamCircle) {
	surface.StrokeCircle(circle.Center.X, circle.Center.Y, circle.Radius)
	// surface.FillCircle(circle.Center.X, circle.Center.Y, circle.Radius)
	if circle.parent != nil {
		// surface.SetLineWidth(circle.Radius)
		surface.MoveTo(circle.parent.Center.X, circle.parent.Center.Y)
		surface.LineTo(circle.Center.X, circle.Center.Y)
		surface.Stroke()
	}
}
